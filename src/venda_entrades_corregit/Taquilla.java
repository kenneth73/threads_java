package venda_entrades_corregit;

public class Taquilla {
    int totalEntrades;

    public Taquilla(int totalEntrades) {
        this.totalEntrades = totalEntrades;
    }

    public synchronized void reservaEntrades(String nom, int entrades) {
        System.out.println("--- ... PROCENSAN LA VENTA ... ---");
        System.out.println("En " + nom + " vol reservar " + entrades + " entrades...");
        if (entrades>4) {
            System.out.println("ERROR, no esta permes reservar mes de " + 4 + " entrades,");
        } else if (totalEntrades>0 && totalEntrades>= entrades) {
            System.out.println("S'han reservat " + entrades + " entrades.");
            totalEntrades -= entrades;
        }else {
            System.out.println("ERROR, solament quedan "+totalEntrades + " entrades");
        }
        System.out.println("ENTRADES RESTANTS = " + totalEntrades);
        System.out.println("--- ... VENTA FINALITZADA ... ---");
    }
}
