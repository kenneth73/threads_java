package venda_entrades_corregit;

public class TestThread {

    public static void main(String[] args) {
        Taquilla taquilla = new Taquilla(10);
        //ThreadCompra t1 = new ThreadCompra(taquilla, "Claudio", 10);
        //ThreadCompra t2 = new ThreadCompra(taquilla, "Kenneth", 10);
        //ThreadCompra t3 = new ThreadCompra(taquilla, "Kilian", 10);
        ThreadCompra t4 = new ThreadCompra(taquilla, "Christian", 10);
        ThreadCompra t5 = new ThreadCompra(taquilla, "MJ", 4);
        ThreadCompra t6 = new ThreadCompra(taquilla, "James", 4);
        ThreadCompra t7 = new ThreadCompra(taquilla, "Flavia", 3);
        ThreadCompra t8 = new ThreadCompra(taquilla, "Alex", 2);
        ThreadCompra t9 = new ThreadCompra(taquilla, "Sonia", 1);
        //t1.start();
        //t2.start();
        //t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();

    }

}
