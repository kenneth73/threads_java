package venda_entrades_corregit;

public class ThreadCompra extends Thread{
    Taquilla taquilla;
    String nom;
    int entrades;

    public ThreadCompra(Taquilla t, String nom, int entrades) {
        this.taquilla = t;
        this.nom = nom;
        this.entrades = entrades;
    }

    @Override
    public void run() {
        taquilla.reservaEntrades(nom, entrades);
    }
}
