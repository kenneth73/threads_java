package pass_missatges.exemple_wait_notify;

public class Exemple2WaitNotify {
    private static volatile String message;
    public static void main (String[] args) {
        Thread thread1 = new Thread(() -> {
            while (message == null) {
                System.out.println("En null!");
            }
            System.out.println(message);
        });
        Thread thread2 = new Thread(() -> {
            message = "A message from thread1";
        });
        thread1.start();
        thread2.start();
    }
}
