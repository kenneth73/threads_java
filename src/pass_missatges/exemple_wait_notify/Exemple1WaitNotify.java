package pass_missatges.exemple_wait_notify;

public class Exemple1WaitNotify{
    private static String message;
    public static void main (String[] args) {
        Thread thread1 = new Thread(() -> {
            System.out.println(message);
        });
        Thread thread2 = new Thread(() -> {
            message = "A message from thread1";
        });
        thread1.start();
        thread2.start();
        /*try {
            thread2.sleep(1);
            thread1.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

    }
}
