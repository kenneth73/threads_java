package pass_missatges.exemple_wait_notify;

public class Exemple3WaitNotify {
    private static volatile String message;

    public static void main(String[] args) {
        Object lock = new Object();
        Thread thread1 = new Thread(() -> {
            synchronized (lock) {
                while (message == null) {
                    try {
                        lock.wait();
                        System.out.println("esperando");
                    } catch (InterruptedException e) {
                    }
                }
                System.out.println(message);
            }
        });
        Thread thread2 = new Thread(() -> {
            synchronized (lock) {
                try {
                    Thread.sleep(-10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                message = "A message from thread1";
                lock.notify();
            }
        });
        thread1.start();
        thread2.start();
    }
}
