package compta_ovelles;

import java.time.LocalTime;

public class ProvaLambda {
    private static int ovelles = 0;
    private static LocalTime ara = LocalTime.now();
    private static boolean esHora= false;

    public static void main(String[] args) {


        LocalTime despertar = ara.plusSeconds(10);


        Runnable comptadorRunnable = new Runnable() {
            @Override
            public void run() {
                while (!esHora) {
                    ovelles++;
                    System.out.println(ovelles + " ovelles");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {}
                }
                Thread.interrupted();
            }
        };

        Runnable despertadorRunnable = () -> {
            while (despertar.isAfter(ara)){
                System.out.println("Encara no es l'hora...");
                ara = LocalTime.now();
                System.out.println("Ara: " + ara);
                System.out.println("Hora de despertar = " + despertar);
                esHora= true;
            }

            System.out.println("RIIIIIIIIINGGG");

        };

        Thread t1 = new Thread(comptadorRunnable);
        Thread t2 = new Thread(despertadorRunnable);

        t1.start();
        t2.start();
    }
}




