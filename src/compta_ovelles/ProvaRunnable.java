package compta_ovelles;

import java.time.LocalTime;

public class ProvaRunnable {
    private static Thread threadDespertador ;
    private static Thread threadComptador;
    public static void main(String[] args) {
        LocalTime now = LocalTime.now();

        RunnableDespertador runnableDespertador = new RunnableDespertador(now.plusSeconds(5));
        RunnableComptador runnableComptador = new RunnableComptador();

        threadDespertador = new Thread(runnableDespertador);
        threadComptador = new Thread(runnableComptador);

        threadDespertador.start();
        threadComptador.start();

    }


}
