package compta_ovelles;

import java.time.LocalTime;

public class ThreadDespertador extends  Thread{

    private LocalTime horaDespertar;

    public ThreadDespertador(LocalTime horaDespertar) {
        this.horaDespertar = horaDespertar;
    }

    @Override
    public void run() {
        LocalTime now = LocalTime.now();
        super.run();

        while (!horaDespertar.equals(now)) {
            System.out.println("Soc el despertador, encara no he de sonar");
        }
        System.out.println("Hora de despertar!");
        Thread.interrupted();
    }
}
