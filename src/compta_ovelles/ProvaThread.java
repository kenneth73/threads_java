package compta_ovelles;

import java.time.LocalTime;

public class ProvaThread {

    public static void main(String[] args) {

        LocalTime horaDespertador = LocalTime.now();

        ThreadComptador t1 = new ThreadComptador();

        ThreadDespertador t2 = new ThreadDespertador(horaDespertador.plusSeconds(10));

        t1.start();

        System.out.println(horaDespertador);
        System.out.println();
    }

}
