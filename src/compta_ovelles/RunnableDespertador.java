package compta_ovelles;

import java.time.LocalTime;

public class RunnableDespertador implements Runnable{
    public LocalTime horaDespertar;
    public LocalTime horaAra;

    public RunnableDespertador(LocalTime horaDespertar) {
        this.horaDespertar = horaDespertar;
        horaAra = LocalTime.now();
    }

    @Override
    public void run() {
        while (horaDespertar.isAfter(horaAra)) {
            System.out.println("Encara no es l'hora...");
            horaAra = LocalTime.now();
            System.out.println("Ara: " + horaAra);
            System.out.println("Hora de despertar = " + horaDespertar);
        }
        Thread.interrupted();
        //s'hauria de interrumpir el thread de RunnableComptador!
        System.out.println("RIIIIIIIIINGGG");
    }

    public static void interrompre(Thread t) {
        t.interrupt();
    }
}
