package bicicleta;
import java.time.LocalTime;

public class ProvaBiciRunnable {

    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;

        BiciThread b1 = new BiciThread("Messi", DISTANCIA, inici);
        BiciThread b2 = new BiciThread("Ronaldo", DISTANCIA, inici);
        BiciThread b3 = new BiciThread("Indurain", DISTANCIA, inici);
        //crear los threads con los objetos bici
        Thread t1 = new Thread(b1);
        Thread t2 = new Thread(b2);
        Thread t3 = new Thread(b3);

        t1.start();
        t2.start();
        t3.start();

        try{
            t1.join();
            t2.join();
            t3.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String mesRapid = b1.getNom();
        long min = b1.getTemps();

        if (b2.getTemps() < min ){
            min = b2.getTemps();
            mesRapid= b2.getNom();
        }
        if (b3.getTemps() < min) {
            mesRapid= b3.getNom();
        }
        System.out.println("El primer en acabar es " + mesRapid);


    }


}
