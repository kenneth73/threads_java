package bicicleta;

import java.time.LocalTime;

public class ProvaBiciThread {
    public static void main(String[] args)  {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;
        BiciThread b1 = new BiciThread("Messi", DISTANCIA, inici);
        BiciThread b2 = new BiciThread("Ronaldo", DISTANCIA, inici);
        BiciThread b3 = new BiciThread("Indurain", DISTANCIA, inici);

        b1.start();
        b2.start();
        b3.start();

        try{
            b1.join();
            b2.join();
            b3.join();
        } catch (InterruptedException e){ e.printStackTrace();}

        /* sense concurrencia
        b1.run();
        b2.run();
        b3.run();*/


        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);

        if (b1.getTemps()< b2.getTemps() && b1.getTemps()<b3.getTemps()){
            System.out.println("Bici mes rapida:" +b1.getNom() );
        }
        if (b2.getTemps()< b1.getTemps() && b2.getTemps()<b3.getTemps()){
            System.out.println("Bici mes rapida:" +b2.getNom() );
        }
        if (b3.getTemps()< b1.getTemps() && b3.getTemps()<b2.getTemps()){
            System.out.println("Bici mes rapida:" +b3.getNom() );
        }
    }

}
