package bicicleta;

import java.time.Duration;
import java.time.LocalTime;

public class ExempleLambda {
    public static void main(String[] args) {
        //lambda Runnable
        Runnable runnable = () -> {
            LocalTime inici = LocalTime.now();

            System.out.println("\n"+ "Comença el thread "+Thread.currentThread().getName());
            for (int i = 0; i < 100; i++) {
                //System.out.println(Thread.currentThread().getName() + " iteration " + i);
            }

            LocalTime end = LocalTime.now();
            long total= Duration.between(inici,end).getNano();

            System.out.println("Temps transcorregut de " + Thread.currentThread().getName() + " : " +total);


        };

        Thread t1 = new Thread(runnable);
        Thread t2 = new Thread(runnable);

        Thread t3 = new Thread(() -> {
            LocalTime inici = LocalTime.now();

            System.out.println("\n"+ "Comença el thread "+Thread.currentThread().getName());
            for (int i = 0; i < 100; i++) {
                //System.out.println(Thread.currentThread().getName() + " iteration " + i);
            }

            LocalTime end = LocalTime.now();
            long total= Duration.between(inici,end).getNano();

            System.out.println("Temps transcorregut de " + Thread.currentThread().getName() + " : " +total);

        });

        //Thread lambda anonima
        new Thread(() -> {
            LocalTime inici = LocalTime.now();

            System.out.println("\n"+ "Comença el thread "+Thread.currentThread().getName());
            for (int i = 0; i < 100; i++) {
                //System.out.println(Thread.currentThread().getName() + " iteration " + i);
            }

            LocalTime end = LocalTime.now();
            long total= Duration.between(inici,end).getNano();

            System.out.println("Temps transcorregut de " + Thread.currentThread().getName() + " : " +total);

        }).start();
        t1.start();
        t2.start();
        t3.start();

    }
}