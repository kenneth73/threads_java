package bicicleta;

import java.time.Duration;
import java.time.LocalTime;

public class RunnableAnonima {

    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                LocalTime inici = LocalTime.now();
                System.out.println("\n"+ "Comença el thread "+Thread.currentThread().getName());
                for(int i=0;i<100;i++){
                    System.out.println(i+" "+Thread.currentThread().getName());
                }
                LocalTime end = LocalTime.now();
                long total= Duration.between(inici,end).getNano();

                System.out.println("Temps transcorregut de " + Thread.currentThread().getName() + " : " +total);
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("\n"+ "Comença el thread "+Thread.currentThread().getName());
                for(int i=0;i<100;i++){
                    System.out.println(i+" "+Thread.currentThread().getName());
                }
            }
        });

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("\n"+ "Comença el thread "+Thread.currentThread().getName());
                for(int i=0;i<100;i++){
                    System.out.println(i+" "+Thread.currentThread().getName());
                }
            }
        });

        /*t1.start();
        t2.start();
        t3.start();*/

        /* Perque el thread es el main??? pasan a ser del thread main?*/
        t1.run();
        t2.run();
        t3.run();
    }
}
