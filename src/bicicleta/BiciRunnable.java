package bicicleta;

import java.time.Duration;
import java.time.LocalTime;

public class BiciRunnable implements Runnable{

    private String nom;//nom del propietari de la bici

    private LocalTime inici;//moment en què comença el viatge en bici

    private int distancia;// distància a recórrer

    private long temps;//temps transcorregut entre inici i completar la distància

    public BiciRunnable(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }
    @Override
    public void run() {
        System.out.println("\nBicicleta del " + nom + " està en marxa dins el thread "+ Thread.currentThread().getName());
        for(int i=0;i<distancia;i++){
            //System.out.println("Distancia bici " + nom +" = "+ i);
        }
        LocalTime horaFinal = LocalTime.now();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long aux= Duration.between(inici,horaFinal).getNano();
        temps = aux;

        System.out.println("Temps transcorregut de " + nom + " : " +(aux/1000000));

    }

    public String getNom() {
        return nom;
    }

    public long getTemps() {
        return temps;
    }

    @Override
    public String toString() {
        return "versio1.Bici{" +
                "nom='" + nom + '\'' +
                ", inici=" + inici +
                ", distancia=" + distancia +
                ", temps=" + temps +
                '}';
    }
}
