package pruebas;

public class Main {
    public static void main(String[] args) {
        //instanciando el Thread con una clase Runnable
        new Thread(new HelloRunnable()).start();

        //con una clase anonima
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello from a thread!");
            }
        }).start();


    }
    static class HelloRunnable implements Runnable {
        public void run() {
            System.out.println("Hello from a thread!");
        }
    }
}
