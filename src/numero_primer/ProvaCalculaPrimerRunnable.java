package numero_primer;

public class ProvaCalculaPrimerRunnable {
    public static void main(String[] args) {
        CalculaPrimerRunnable e1 = new CalculaPrimerRunnable(100);
        CalculaPrimerRunnable e2 = new CalculaPrimerRunnable(1000);
        CalculaPrimerRunnable e3 = new CalculaPrimerRunnable(1000000000);

        Thread t1 = new Thread(e1);
        Thread t2 = new Thread(e2);
        Thread t3 = new Thread(e3);

        t1.start();
        t2.start();
        t3.start();
    }
}
