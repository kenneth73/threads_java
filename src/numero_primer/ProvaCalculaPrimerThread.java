package numero_primer;

import java.util.Scanner;

public class ProvaCalculaPrimerThread {
    public static void main(String[] args) {
        CalculaPrimerThread t1 = new CalculaPrimerThread(20);
        CalculaPrimerThread t2 = new CalculaPrimerThread(200);
        CalculaPrimerThread t3 = new CalculaPrimerThread(2000);

        t1.start();
        t2.start();
        t3.start();
    }
    public static int inputInt(String message) {
        Scanner reader = new Scanner(System.in);
        System.out.print(message);

        int num = 0;
        boolean isNum;
        do {
            try {
                num = reader.nextInt();
                isNum = true;
            } catch (Exception e) {
                System.out.print(message);
                isNum = false;
            }
            reader.nextLine();
        } while (!isNum);


        return num;
    }
}
