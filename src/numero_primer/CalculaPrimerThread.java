package numero_primer;

public class CalculaPrimerThread extends Thread{

    private long primerEnter;

    public CalculaPrimerThread(long primerEnter) {
        this.primerEnter = primerEnter;
    }

    static boolean esPrimer(long n) {
        int i;
        for(i=2;i<(int)(n);i++) {
            if(n%i==0)
                return false;
        }
        return true;
    }

    static void seguentPrimer(long primer) {
        long seguentPrimer = primer;
        boolean trobat = false;
        while (!trobat) {
            seguentPrimer+= 1;
            if (esPrimer(seguentPrimer)){
                System.out.println("El seguent primer es "+ seguentPrimer + "("+ Thread.currentThread().getName() +")");
                trobat = true;
            }
        }
    }

    @Override
    public void run() {
        super.run();
        seguentPrimer(primerEnter);
    }
}
