package exemple_bloquingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(10);
        //int coreCount = Runtime.getRuntime().availableProcessors();
        //System.out.println(coreCount);
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(new ProductorNumeros(blockingQueue));

        //executorService.submit(new ProductorNumeros(blockingQueue));

        executorService.submit(new ConsumidorNumeros(blockingQueue));

    }
}
