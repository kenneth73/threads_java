package exemple_bloquingQueue;

import java.util.concurrent.BlockingQueue;

public class ConsumidorNumeros implements Runnable{

    private BlockingQueue<Integer> blockingQueue;

    public ConsumidorNumeros(BlockingQueue<Integer> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int num = blockingQueue.take();
                System.out.println("Numero consumit en el Thread "+Thread.currentThread().getName()+" = " + num);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
