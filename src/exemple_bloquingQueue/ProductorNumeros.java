package exemple_bloquingQueue;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class ProductorNumeros implements Runnable{
    private BlockingQueue<Integer> blockingQueue;

    public ProductorNumeros(BlockingQueue<Integer> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {

        Random r = new Random();
        while (true) {
            try {
                int newNum = r.nextInt(20);

                blockingQueue.put(newNum);
                System.out.println("Numero produit en el Thread = "+Thread.currentThread().getName()+" = " + newNum);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
