package fitxer_de_text;

import java.io.File;
import java.util.Scanner;

public class Exercici1Runnable implements Runnable{

    private String nomFitxer = "dades.txt";

    public void comptaLineas() {
        File file = new File(nomFitxer);
        int linies = 0;

        try {
            Scanner lector = new Scanner(file);
            while (lector.hasNextLine()){
                lector.nextLine();
                linies++;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Thread "+Thread.currentThread().getName() +": " + linies + " linies.");
    }

    @Override
    public void run() {
        comptaLineas();
    }
}
