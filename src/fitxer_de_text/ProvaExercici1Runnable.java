package fitxer_de_text;

public class ProvaExercici1Runnable {
    public static void main(String[] args) {
        Exercici1Runnable e1 = new Exercici1Runnable();
        Exercici1Runnable e2 = new Exercici1Runnable();
        Exercici1Runnable e3 = new Exercici1Runnable();

        Thread t1 = new Thread(e1);
        Thread t2 = new Thread(e2);
        Thread t3 = new Thread(e3);

        t1.start();
        t2.start();
        t3.start();

        /*try{
            t1.join();
            t2.join();
            t3.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/


    }
}
