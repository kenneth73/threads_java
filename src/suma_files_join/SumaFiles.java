package suma_files_join;

import java.util.concurrent.atomic.AtomicInteger;

public class SumaFiles extends Thread{
    int[] fila;
    public int total=0;

    public SumaFiles(int[] fila) {
        this.fila = fila;
    }

    @Override
    public void run() {
        for (int x : fila)
            total+=x;
    }

    public int getTotal() {
        return total;
    }
}
