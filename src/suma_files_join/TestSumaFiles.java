package suma_files_join;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class TestSumaFiles {
    public static int[][] matriu = new int[10000][10000];
    public static int total = 0;
    public static void main(String[] args) {

        inicialitzaMatriu();
        ArrayList<SumaFiles> arrayList = new ArrayList<>();

        for (int[] fila: matriu)
            arrayList.add(new SumaFiles(fila));


        arrayList.forEach((thread)-> {
            thread.start();
        });


        arrayList.forEach((thread)-> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


        arrayList.forEach((thread)->{
            total+= thread.getTotal();
        });

        System.out.println("Total = "+ total);



    }

    public static void inicialitzaMatriu() {
        for (int i = 0; i < matriu.length; i++) {
            for (int j = 0; j < matriu[i].length; j++) {
                matriu[i][j] = 1;
                //System.out.print(matriu[i][j]);
            }
            //System.out.println();
        }
    }
}
