package suma_files_join.matrices;

public class SumarFilasMatriz {
    private static int sumaTotalTaula;

    public static void main(String[] args) {
        int [][] matriz = new int[100][100];

        for (int i = 0; i < matriz.length; i++){
            for (int j = 0; j < matriz.length; j++){
                matriz[i][j] = (int)(1);
            }
        }


        for (int i = 0; i < matriz.length; i++){
            int fila = i;
            Runnable r = () -> sumarFilas(fila, matriz);
            Thread t = new Thread(r);
            t.start();
            System.out.println("Thread en ejecución "+ t.getName());

            /*try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }
        System.out.println("La suma total de todos los valores de la matriz es " + sumaTotalTaula);
    }


    public static void sumarFilas(int fila, int[][] matriz){
        int valor = 0;
        for (int i = 0; i < matriz[fila].length; i++) {
            valor = valor + matriz[fila][i];
            sumaTotalTaula = sumaTotalTaula + matriz[fila][i];
        }
        System.out.println("Suma fila " + fila + "= " + valor);
    }
}



