package productor_consumidor;

public class TestProductorConsumidor {

    public static void main(String[] args) {
        Camarero camarero = new Camarero();
        Productor productor = new Productor(camarero);
        Consumidor consumidor = new Consumidor(camarero);

        Thread threadProductor = new Thread(productor);
        Thread threadConsumidor = new Thread(consumidor);

        threadProductor.start();
        threadConsumidor.start();
    }


}
