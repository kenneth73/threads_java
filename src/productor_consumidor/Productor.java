package productor_consumidor;

public class Productor implements Runnable{
    public Camarero camarero;

    public Productor(Camarero camarero) {
        this.camarero = camarero;
    }

    @Override
    public void run() {
        String[] tapas = {"Jamon", "Queso", "Patatas", "Olivas"};

        for (int i = 0; i <tapas.length ; i++) {
            camarero.dejar(tapas[i]);
            System.out.println("Plato preparado! " + tapas[i]);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        camarero.dejar("Todos los platos servidos.");
    }
}
