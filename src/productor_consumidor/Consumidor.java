package productor_consumidor;

public class Consumidor implements Runnable{
    Camarero camarero;

    public Consumidor(Camarero camarero) {
        this.camarero = camarero;
    }

    @Override
    public void run() {
        String tapa = "";
        while (!tapa.equals("Todos los platos servidos.")) {
            tapa = camarero.cojer();
            System.out.println("Tapa servida: " + tapa);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
