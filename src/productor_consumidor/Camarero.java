package productor_consumidor;

public class Camarero {
    private String tapa;
    private boolean vandejaVacia = true;

    public synchronized String cojer() {
        //mientras la vandeja esta vacia, nos esperamos a recibir el notify
        while (vandejaVacia){
            try {
                System.out.println("Esperando a cojer el plato");
                wait();
                System.out.println("*************************");
            } catch (InterruptedException e) { e.printStackTrace(); }
        }
        //la vandeja la han llenado, cojemos el plato, por eso la volvemos a poner vacia
        vandejaVacia = true;
        //notificamos que ya hemos recogido el plato
        notify();
        return tapa;
    }

    public synchronized void dejar(String tapaDejada) {
        //mientras la vandeja este llena, no podemos hacer nada
        while (!vandejaVacia) {
            try {
                System.out.println("Esperando a dejar el plato");
                wait();
            } catch (InterruptedException e) { e.printStackTrace(); }
        }
        //la vandeja esta vacia, podemos dejar el plato
        vandejaVacia = false;
        //aqui dejamos el plato
        tapa = tapaDejada;
        //notificamos que lo hemos dejado
        notify();
    }
}
