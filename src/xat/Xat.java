package xat;
public class Xat {
    private boolean isAnswer = false;

    public synchronized void ferPregunta(String p) {
        while (isAnswer) {
            try {
                wait();
            } catch (InterruptedException e) {e.printStackTrace();}
        }
        isAnswer =true;
        System.out.println(p);
        notify();
    }

    public synchronized void contestar(String r) {
        while (!isAnswer) {
            try {
                wait();
            } catch (InterruptedException e) {e.printStackTrace();}
        }
        isAnswer = false;
        //hasResponse= true;
        System.out.println(r);
        notify();
    }
}
