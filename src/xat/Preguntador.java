package xat;
public class Preguntador implements Runnable{
    public Xat xat;

    public Preguntador(Xat xat) {
        this.xat = xat;
    }

    @Override
    public void run() {
        String[] preguntas = {"1+1= ?","2+2= ?","3+3= ?"};
        for (String p: preguntas){
            xat.ferPregunta(p);
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e) { e.printStackTrace(); }
        }
        System.out.println("Fin de la conversacion!");
    }
}
