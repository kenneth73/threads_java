package xat;

public class TestXat {
    public static void main(String[] args) {
        Xat xat = new Xat();
        Runnable preg = new Preguntador(xat);
        Runnable resp = new Respondedor(xat);

        Thread threadPreg = new Thread(preg);
        Thread threadResp = new Thread(resp);

        threadPreg.start();
        threadResp.start();
    }
}
