package xat;
public class Respondedor implements Runnable{
    Xat xat;

    public Respondedor(Xat xat) {
        this.xat = xat;
    }

    @Override
    public void run() {
        String[] respuestas = {"2", "4", "6"};
        for (String r : respuestas){
            xat.contestar(r);
            try{
                Thread.sleep(1000);
            } catch (InterruptedException e) { e.printStackTrace();}
        }
    }
}
