package executors_i_callables.daus;

import java.util.concurrent.Callable;

public class Dau implements Callable<Integer> {
    private int tirada;

    public int llençarDau() {
        tirada = (int) (Math.random() * 5 + 1);
        System.out.println("Tirada " + Thread.currentThread().getName() + " : " + tirada);
        return tirada;
    }

    public int getTirada() {
        return tirada;
    }

    @Override
    public Integer call() throws Exception {
        return llençarDau();
    }
}
