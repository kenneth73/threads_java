package executors_i_callables.callable_daus_montse;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestDau {

    public static void main(String[] args) {
        Dau d1 = new Dau();
        Dau d2 = new Dau();
        Dau d3 = new Dau();

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        Future<Integer> futurValor1 = executorService.submit(d1);
        Future<Integer> futurValor2 = executorService.submit(d2);
        Future<Integer> futurValor3 = executorService.submit(d3);
        try {
            int v1 = futurValor1.get();
            int v2 = futurValor2.get();
            int v3 = futurValor3.get();
            System.out.println("Valor de les 3 tirades: " +(v1+v2+v3));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }



    }
}
