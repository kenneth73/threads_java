package executors_i_callables.callable_daus_montse;

import java.util.concurrent.Callable;

public class Dau implements Callable<Integer> {

    public int llanzarDado() {
        int tirada = (int) (Math.random()*5+1);
        System.out.println("Tirada" + Thread.currentThread().getName() + " = " + tirada);
        return tirada;
    }

    @Override
    public Integer call() {
        return llanzarDado();
    }
}
