package executors_i_callables.suma_filas_callable;

import java.util.concurrent.Callable;

public class SumaFilaCallable implements Callable<Integer> {
    int sumatori;
    int[] fila;

    public SumaFilaCallable(int[] fila) {
        this.fila = fila;
    }

    public int sumaFila() {
        for (int v : fila)
            sumatori+= v;
        return sumatori;
    }

    @Override
    public Integer call() throws Exception {
        return sumaFila();
    }
}
