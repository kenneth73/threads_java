package executors_i_callables.suma_filas_callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestSumaFiles {

    public static void main(String[] args) {
        //cada fila suma 12
        int[][] matiu = {{1,1,1,1,1,1,1,1,1,1,1,1,},{2,1,2,1,2,1,2,1,},{3,3,3,3,},{3,3,3,3,}};
        SumaFilaCallable[] arrayCallables = new SumaFilaCallable[matiu.length];
        Future[] futures = new Future[matiu.length];
        ExecutorService executorService = Executors.newCachedThreadPool();
        int sumatoriMatriu = 0;

        for (int i = 0; i <matiu.length ; i++) {
            arrayCallables[i] = new SumaFilaCallable(matiu[i]);
            futures[i] = executorService.submit(arrayCallables[i]);
        }

        try{
            for (int i = 0; i < matiu.length; i++) {
                //System.out.println(futures[i].get());
                sumatoriMatriu += (int) futures[i].get();
            }

            System.out.println("El sumatori es " + sumatoriMatriu);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }



    }

}
