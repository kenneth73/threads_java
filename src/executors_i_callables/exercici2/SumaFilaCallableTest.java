package executors_i_callables.exercici2;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SumaFilaCallableTest {
    public static int[][] matriu = new int[10][10];
    public static int sumatori = 0;

    public static void main(String[] args) {

        inicialitzaMatriu();
        SumaFilaCallable[] callables = new SumaFilaCallable[matriu.length];
        Future[] futures = new Future[matriu.length];

        ExecutorService executorService = Executors.newCachedThreadPool();

        for (int i = 0; i < matriu.length; i++) {
            callables[i] = new SumaFilaCallable(matriu[i]);
            futures[i] = executorService.submit(callables[i]);
        }

        try {

            for (int i = 0; i < matriu.length; i++) {
                sumatori += (int) futures[i].get();
            }

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println(sumatori);
    }

    public static void inicialitzaMatriu() {
        for (int i = 0; i < matriu.length; i++) {
            for (int j = 0; j < matriu[i].length; j++) {
                matriu[i][j] = 1;
                System.out.print(matriu[i][j]);
            }
            System.out.println();
        }
    }
}
