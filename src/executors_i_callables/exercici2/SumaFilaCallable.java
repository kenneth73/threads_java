package executors_i_callables.exercici2;

import java.util.concurrent.Callable;

public class SumaFilaCallable implements Callable<Integer> {

    private int[] fila;
    private int sumatori=0;

    public SumaFilaCallable(int[] fila) {
        this.fila = fila;
    }

    private Integer sumarFila() {
        for (int x: fila)
            sumatori+=x;
        return sumatori;
    }

    @Override
    public Integer call() throws Exception {
        return sumarFila();
    }
}
