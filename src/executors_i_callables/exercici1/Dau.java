package executors_i_callables.exercici1;

import java.util.Random;
import java.util.concurrent.Callable;

public class Dau implements Callable<Integer> {



    @Override
    public Integer call() throws Exception {
        int tirada = (int) (Math.random() * 5 + 1);
        System.out.println("EL dau del Thread " + Thread.currentThread().getName() + "val:" + tirada );
        return tirada;
    }
}
