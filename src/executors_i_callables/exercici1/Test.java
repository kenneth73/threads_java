package executors_i_callables.exercici1;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Test {

    public static void main(String[] args) {
        Dau dau1 = new Dau();
        Dau dau2 = new Dau();
        Dau dau3 = new Dau();

        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<Integer> f1 = executorService.submit(dau1);
        Future<Integer> f2 = executorService.submit(dau2);
        Future<Integer> f3 = executorService.submit(dau3);

        try {
            System.out.println("Dau 1= " + f1.get());
            System.out.println("Dau 2= " + f2.get());
            System.out.println("Dau 3= " + f3.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }




    }
}
