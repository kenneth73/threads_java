package executors_i_callables.operacions_executor;

import java.util.concurrent.*;

public class Exemple1Executors {
    public static void main(String[] args) {
        System.out.println("Dintre de: " + Thread.currentThread().getName());
        System.out.println("Creant un ExecutorService...");
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        System.out.println("Creant un objecte Runnable...");
        Callable runnable = () -> {
            System.out.println("Dintre : " + Thread.currentThread().getName());
            for (int i = 0; i < 5; i++) {
                System.out.println("Dintre : " + Thread.currentThread().getName() + " contan numerets..." + i);
            }
            return 0;
        };
        System.out.println("Enviar la tasca de l'objecte runnable a l'executor service.");
        Future<Integer> f = executorService.submit(runnable);
        try {
            System.out.println(f.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("Tancant l'executor"); //si no ho fem es queda fent run
        executorService.shutdown();
        System.out.println("Executor tancat");
    }
}
