package executors_i_callables.mida_directori;

import java.io.File;
import java.util.concurrent.Callable;

public class MeasureSizeFile implements Callable<Long> {

    public File file;

    public MeasureSizeFile(File file) {
        this.file = file;
    }

    @Override
    public Long call() {
     return file.length();
    }
}