package executors_i_callables.mida_directori;

import java.util.concurrent.*;

public class MeasureSizeDirectoryTest {

    public static void main(String[] args) {
        long startTime = System.nanoTime();

        String path ="D:\\Kenneth";
        //String path ="D:\\Kenneth\\Documents\\ITB";

        MeasureSizeDirectory callable = new MeasureSizeDirectory(path);

        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<Long> f1 = executorService.submit(callable);


        try {
            Long v1 = f1.get();
            System.out.println("Path: " + path);
            System.out.println(v1/1024+ " KB");
            System.out.println(v1+ " Bytes");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        long stopTime = System.nanoTime();
        System.out.println("TIMMING");
        System.out.println(stopTime - startTime);

    }

}
