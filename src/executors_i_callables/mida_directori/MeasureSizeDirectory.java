package executors_i_callables.mida_directori;

import java.io.File;
import java.util.concurrent.Callable;

public class MeasureSizeDirectory implements Callable<Long> {

    private static long tamanioDirectorio=0;

    public String path;



    public MeasureSizeDirectory(String path) {
        this.path = path;
    }

    public static void printSizeDirectory(String path) {

        File file = new File(path);
        File[] files = file.listFiles();
        if (files != null & files.length>0) {
            for (File f : files) {
                if (f.isDirectory()) {
                    //System.out.println("Directori: " + f.getName());
                    printSizeDirectory(f.getAbsolutePath()); // recursive call
                } else {
                    //System.out.println("  File name: "+ f.getName() + " File length: " + f.length()/1024 + " KB");
                    tamanioDirectorio += f.length();
                }
            }
        }
    }

    @Override
    public Long call() {
        printSizeDirectory(path);
        return tamanioDirectorio;
    }
}