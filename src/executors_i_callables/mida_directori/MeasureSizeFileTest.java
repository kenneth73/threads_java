package executors_i_callables.mida_directori;

import java.io.File;
import java.util.concurrent.*;

public class MeasureSizeFileTest {
    public static Long sizeDirectori = (long) 0;
    public static ExecutorService executorService = Executors.newCachedThreadPool();

    public static void main(String[] args) {

        long startTime = System.nanoTime();


        String path ="D:\\Kenneth";
        //String path ="D:\\Kenneth\\Documents\\ITB";

        File file = new File(path);
        measureSizeDirectory(file);

        System.out.println(sizeDirectori /1024+ " KB");
        System.out.println(sizeDirectori + " Bytes");


        long stopTime = System.nanoTime();
        System.out.println("TIMMING");
        System.out.println(stopTime - startTime);

    }

    public static void measureSizeDirectory(File file) {
        File[] files = file.listFiles();

        if (files != null) {
            for (File f : files) {
                if (f.isDirectory()) {
                    measureSizeDirectory(f);// recursive call
                } else {
                    MeasureSizeFile callable = new MeasureSizeFile(f);
                    Future<Long> future = executorService.submit(callable);
                    try {
                        sizeDirectori += future.get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
